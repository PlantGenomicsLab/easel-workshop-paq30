#!/bin/bash
#SBATCH --job-name=mikado
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=user.email@uconn.edu
#SBATCH --mem=10G
#SBATCH -o mikado_%j.out
#SBATCH -e mikado_%j.err

source activate mikado_env

species=species
reference=species_reference_annotation.gtf 
prediction=annotation_prediction.gff3

mikado compare -r $reference -p $prediction -pc -eu -erm -o $species

